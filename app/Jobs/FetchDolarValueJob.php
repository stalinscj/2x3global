<?php

namespace App\Jobs;

use App\Payment;
use App\Services\MindicadorService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class FetchDolarValueJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $payment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mindicadorService = new MindicadorService();
        $date = $this->payment->created_at;

        $payment = Payment::whereDate('created_at', $date)
            ->whereNotNull('clp_usd')
            ->first();

        $dolarValue = $payment ? $payment->clp_usd : $mindicadorService->getDolarValue($date->format('d-m-Y'));

        $this->payment->clp_usd = $dolarValue;
        $this->payment->save();
    }
}
