<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends User
{

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
