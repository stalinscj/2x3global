<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePaymentRequest;
use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $payments = !$request->has('client_id') ? Payment::all() : Payment::where('user_id', $request->client_id)->get();

        return response()->json($payments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePaymentRequest $request)
    {
        $payement = Payment::create($request->only('payment_date', 'expires_at', 'status', 'user_id'));

        return response()->json($payement->refresh(), 201);
    }
}
