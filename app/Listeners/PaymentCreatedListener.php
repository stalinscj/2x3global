<?php

namespace App\Listeners;

use App\Events\PaymentCreatedEvent;
use App\Jobs\FetchDolarValueJob;
use App\Mail\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class PaymentCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentCreatedEvent  $event
     * @return void
     */
    public function handle(PaymentCreatedEvent $event)
    {
        $payment = $event->payment;

        $fetchDolarValueJob = new FetchDolarValueJob($payment);
        dispatch($fetchDolarValueJob);

        $client  = $payment->client()->first();
        Mail::to($client->email)->queue(new Notification($client));
    }
}
