<?php

namespace App;

use App\Events\PaymentCreatedEvent;
use App\Events\PaymentCreatingEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Payment extends Model
{

	protected $fillable = [
		'expires_at', 
		'payment_date', 
		'status', 
		'user_id'
	];

	protected $hidden = [
		'id',
		'created_at',
		'updated_at',
	];

	protected static function boot()
    {
        parent::boot();

        static::creating(function ($payment) {
            $payment->uuid = (string) Str::uuid();
        });

        static::created(function ($payment) {
            event(new PaymentCreatedEvent($payment));
        });
    }
    
    public function client()
    {
        return $this->belongsTo(Client::class, 'user_id');
    }

    public function filter($params)
    {
    	$query = Payment::query();


    }
}
