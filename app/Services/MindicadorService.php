<?php

namespace App\Services;

use GuzzleHttp\Client;

class MindicadorService  
{
    protected $client;

    public function __construct() {
        $this->client = new Client(['base_uri' => 'https://mindicador.cl/api/']);

    }

    public function getDolarValue($date)
    {
        $dolarValue = null;

        $response = $this->client->request('GET', "dolar/$date");

        
        if ($response->getStatusCode() == 200) {
    
            $data = json_decode($response->getBody()->getContents(), true);

            if (count($data['serie'])) {
                $dolarValue = $data['serie'][0]['valor'];
            }

        }

        return $dolarValue;
    }

    
}