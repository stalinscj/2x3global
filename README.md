# Instalación

```bash
git clone https://stalinscj@bitbucket.org/stalinscj/2x3global.git
```

```bash
cd 2x3global
```

```bash
composer install
```

```bash
cp .env.example .env
```

```bash
php artisan key:generate
```

Agregar las credenciales de la BBDD en el archivo .env

Agregar las credenciales de Mailtrap en el archivo .env o cambiar MAIL_DRIVER=log

```bash
php artisan migrate --seed
```

```bash
php artisan queue:work
```
